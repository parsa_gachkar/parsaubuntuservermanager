using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace ParsaUbuntuServerManager
{

	public class NetworkManager
	{
		enum inetMode{
			DHCP,STATIC
		}
		public NetworkManager ()
		{
			bool exit = false;
			String interfacesFilePath = "/etc/network/interfaces";

				List<string> lines = File.ReadAllText (interfacesFilePath).Split (new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList<string> ();
				List<string> should_be_removed = new List<string> ();
				List<networkInterface> interfaces = new List<networkInterface> ();

				foreach (var item in lines) {

					if (item.ToLower ().Contains ("auto")) {
						should_be_removed.Add (item);
					}
					if (item.TrimStart () [0] == '#') {
						should_be_removed.Add (item);
					}

				}
				foreach (var item in should_be_removed) {
					lines.Remove (item);

				}
				networkInterface iface = new networkInterface ();

				foreach (var item in lines) {

					if (item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [0] == "iface") {
						if ((iface.name) != null) {
							interfaces.Add (iface);
							iface = new networkInterface ();

						}
						iface.name = item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [1];

						switch (item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [3].ToLower ()) {
						case "loopback":
							iface.mode = 0;
							break;
						case "dhcp":
							iface.mode = 1;
							break;
						case "static":
							iface.mode = 2;
							break;
						default:
							iface.mode = -1;
							break;
						}
					}
					if (item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [0].ToLower () == "address") {
						iface.address = item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [1];
					}
					if (item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [0].ToLower () == "netmask") {
						iface.netmask = item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [1];
					}
					if (item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [0].ToLower () == "gateway") {
						iface.gateway = item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [1];
					}
					if (item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [0].ToLower () == "dns-nameserver") {
						iface.dns_nameserver = item.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) [1];
					}

				}
				if ((iface.name) != null) {
					interfaces.Add (iface);

				}
			while (!exit) {
				ParsaConsoleUtil.line ('-');
				ParsaConsoleUtil.title ("Network interfaces Config");
				ParsaConsoleUtil.print ("\nDetected Interfaces :\n");

				List<string> ints = new List<string> ();
				foreach (var item in interfaces) {

					ints.Add (string.Format ("{0}", item.name));
				}
				int sel = ParsaConsoleUtil.getOption (ints.ToArray (), "Select Intreface to Configure (other key to back)");
				try {
					interfaces [sel - 1].modify ();	
				} catch (Exception ex) {
					exit = true;
				}


			}
			if (ParsaConsoleUtil.prompt ("Write changes to " + interfacesFilePath)) {
				if (ParsaConsoleUtil.prompt ("Backup " + interfacesFilePath)) {
					File.Copy (interfacesFilePath, interfacesFilePath + ".ParsaServerManager.backup");
				}
				//throw new NotImplementedException();
				String output = "";
				output += "auto ";
				String settings = "";
				foreach (var item in interfaces) {
					output += item.name + " ";
					settings += "\niface " + item.name + " inet " + item.getMode(item.mode).ToLower();
					if (item.address != "N/A") {
						settings += "\naddress "+item.address;
					}
					if (item.netmask != "N/A") {
						settings += "\nnetmask "+item.netmask;
					}
					if (item.gateway != "N/A") {
						settings += "\ngateway "+item.gateway;
					}
					if (item.dns_nameserver != "N/A") {
						settings += "\ndns-nameserver "+item.dns_nameserver;
					}
				}
				output += "\n" + settings;
				try {
					//File.WriteAllText(interfacesFilePath,output);
					ParsaConsoleUtil.line('-');
					ParsaConsoleUtil.print(output);
				} catch (Exception ex) {
					ParsaConsoleUtil.print("*Error\t" + ex.Message);
				}

			}
		}

	}

	public class networkInterface{

		public string name= null;
		public int mode= 0;
		public string address= "N/A";
		public string netmask= "N/A";
		public string gateway= "N/A";
		public string dns_nameserver= "N/A";

		public String getMode(int a){
			switch (a) {
				case 0:
				return "Loopback";

				case 1:
				return "DHCP";

				case 2:
				return "Static";

				default:
				return "Unknown";

			}
		}
		public void changeIP(){
			ParsaConsoleUtil.line ('-');
			ParsaConsoleUtil.title (name +" IP settings");
			address = ParsaConsoleUtil.getInput ("IP");
		}
		public void changeNetmask(){
			ParsaConsoleUtil.line ('-');
			ParsaConsoleUtil.title (name +" NetMask settings");
			netmask = ParsaConsoleUtil.getInput ("NetMask");
		}
		public void changeGW(){
			ParsaConsoleUtil.line ('-');
			ParsaConsoleUtil.title (name +" Gateway settings");
			gateway = ParsaConsoleUtil.getInput ("Gateway");
		}
		public void changeDNS(){
			ParsaConsoleUtil.line ('-');
			ParsaConsoleUtil.title (name +" DNS Nameserver settings");
			dns_nameserver = ParsaConsoleUtil.getInput ("DNS Nameserver");
		}
		public void chmod(){
			ParsaConsoleUtil.line ('-');
			ParsaConsoleUtil.title (name +" Mode settings");
			string[] asd = new string[]{"Loopback","DHCP","Static"};
			mode = (ParsaConsoleUtil.getOption (asd, "Select Interface Mode"))-1;
		}
		public void modify(){
			ParsaConsoleUtil.line('-');
			ParsaConsoleUtil.title ("Interface "+name +" Config");
			string[] opt = new string[] {string.Format("IP\t\t({0})",address),string.Format("Mode\t\t({0})",getMode(mode))  , string.Format("NetMask\t\t({0})",netmask) ,string.Format("GateWay\t\t({0})",gateway) , string.Format("DNS-NameServer\t({0})",dns_nameserver) };
			int sel = ParsaConsoleUtil.getOption(opt,"Select your Option (any thing else = back)");
			switch (sel) {
			case 1:
				changeIP ();
				break;
			case 2:
				chmod ();
				break;
			case 3:
				changeNetmask ();
				break;
			case 4:
				changeGW ();
				break;
			case 5:
				changeDNS ();
				break;
			default:
				break;
			}
		}
	}
}


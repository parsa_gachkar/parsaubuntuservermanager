using System;

namespace ParsaUbuntuServerManager
{
	public static class ParsaConsoleUtil
	{
		public static void print(String str){
			Console.WriteLine (str);
		}

		public static void line(char a){
			for (int i = 0; i < 80; i++) {
				Console.Write (a);
			}
		}
		public static void title(string str){
			int a = (80-str.Length)/2;
			string space = "";
			for (int i = 0; i < a; i++) {
				space +=" ";
			}
			print (space+str);
		}

	public static int getOption(String[] str,String query){
		int i = 1;
			foreach (String item in str) {
				print (String.Format("{0})\t{1}",i++,item));
			}
			Console.Write ("\n\n"+query+" : ");
			return int.Parse (getKey().KeyChar.ToString ());
		}
		public static bool prompt(String str){
			print ("\n"+str);
			return getOption(new string[]{"Yes","No"},"Option") == 1;
		}

		public static string getInput(string str){
			Console.Write(str + " : ");
			return Console.ReadLine ();
		}

		public static ConsoleKeyInfo getKey(){
			ConsoleKeyInfo a = Console.ReadKey ();
			print ("");
			return a;

		}
	}
}

